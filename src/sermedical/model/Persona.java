/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sermedical.model;

import java.time.LocalDate;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author tesco01
 */
public class Persona {

    private final IntegerProperty dni;
    private final StringProperty apellido;
    private final StringProperty nombre;
    private final ObjectProperty<LocalDate> fechaNacimiento;
    private final StringProperty sexo;
    private final StringProperty pais;
    private final StringProperty provincia;
    private final StringProperty ciudad;
    private final StringProperty direccion;
    private final StringProperty email;
    
    /**
     * Constructor with some initial data.
     * 
     * @param dni
     * @param apellido
     * @param nombre
     */
    public Persona(int dni, String apellido, String nombre){
        this.dni = new SimpleIntegerProperty(dni);
        this.apellido = new SimpleStringProperty(apellido);
        this.nombre = new SimpleStringProperty(nombre);
        this.fechaNacimiento= null;
        this.sexo = null;
        this.pais = null;
        this.provincia = null;
        this.ciudad = null;
        this.direccion = null;
        this.email = null;
    }
    
    public Persona(int dni, String apellido, String nombre,LocalDate fechaNacimiento  ,String sexo, String pais, String provincia, String ciudad, String direccion, String email){
        this.dni = new SimpleIntegerProperty(dni);
        this.apellido = new SimpleStringProperty(apellido);
        this.nombre = new SimpleStringProperty(nombre);
        this.fechaNacimiento= new SimpleObjectProperty<LocalDate>(fechaNacimiento);
        this.sexo = new SimpleStringProperty(sexo);
        this.pais = new SimpleStringProperty(pais);
        this.provincia = new SimpleStringProperty(provincia);
        this.ciudad = new SimpleStringProperty(ciudad);
        this.direccion = new SimpleStringProperty(direccion);
        this.email = new SimpleStringProperty(email);
        // Some initial dummy data, just for convenient testing.
        //this.street = new SimpleStringProperty("some street");
        //this.birthday = new SimpleObjectProperty<LocalDate>(LocalDate.of(1999, 2, 21));
    }

    public int getDni() {
        return dni.get();
    }
    public void setDni(int dni) {
        this.dni.set(dni);
    }
    public IntegerProperty dniProperty() {
        return dni;
    }
    
    public String getApellido() {
        return apellido.get();
    }
    public void setApellido(String apellido) {
        this.apellido.set(apellido);
    }
    public StringProperty apellidoProperty() {
        return apellido;
    }
    
    public String getNombre() {
        return nombre.get();
    }
    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }
    public StringProperty nombreProperty() {
        return nombre;
    }
    
    public LocalDate getFechaNacimiento() {
        return fechaNacimiento.get();
    }
    public void setBirthday(LocalDate fechaNacimiento) {
        this.fechaNacimiento.set(fechaNacimiento);
    }
    public ObjectProperty<LocalDate> fechaNacimientoProperty() {
        return fechaNacimiento;
    }
    
    public String getSexo() {
        return sexo.get();
    }
    public void setSexo(String sexo) {
        this.sexo.set(sexo);
    }
    public StringProperty sexoProperty() {
        return sexo;
    }
    
    public String getPais() {
        return pais.get();
    }
    public void setPais(String pais) {
        this.pais.set(pais);
    }
    public StringProperty paisProperty() {
        return pais;
    }
    
    public String getProvincia() {
        return provincia.get();
    }
    public void setProvincia(String provincia) {
        this.provincia.set(provincia);
    }
    public StringProperty provinciaProperty() {
        return provincia;
    }
    
    public String getCiudad() {
        return ciudad.get();
    }
    public void setCiudad(String ciudad) {
        this.ciudad.set(ciudad);
    }
    public StringProperty ciudadProperty() {
        return ciudad;
    }
    
    public String getDireccion() {
        return direccion.get();
    }
    public void setDireccion(String direccion) {
        this.direccion.set(direccion);
    }
    public StringProperty direccionProperty() {
        return direccion;
    }
    
    public String getEmail() {
        return email.get();
    }
    public void setEmail(String email) {
        this.email.set(email);
    }
    public StringProperty emailProperty() {
        return email;
    }
    
    
}
