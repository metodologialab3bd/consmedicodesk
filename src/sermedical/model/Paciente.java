/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sermedical.model;

import java.time.LocalDate;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author tesco01
 */
public class Paciente extends Persona {
    private final IntegerProperty idPaciente;

    public Paciente(int idPaciente, int dni, String apellido, String nombre, LocalDate fechaNacimiento, String sexo, String pais, String provincia, String ciudad, String direccion, String email) {
        super(dni, apellido, nombre, fechaNacimiento, sexo, pais, provincia, ciudad, direccion, email);
        this.idPaciente = new SimpleIntegerProperty(idPaciente);
    }
    public Paciente(int idPaciente, int dni, String apellido, String nombre){
         super(dni, apellido, nombre);
         this.idPaciente = new SimpleIntegerProperty(idPaciente);
    }
    
    public int getIdPaciente() {
        return idPaciente.get();
    }
    public void setIdPaciente(int idPaciente) {
        this.idPaciente.set(idPaciente);
    }
    public IntegerProperty IdPacienteProperty() {
        return idPaciente;
    }
    

}
