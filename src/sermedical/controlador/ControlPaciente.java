/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sermedical.controlador;

import java.sql.ResultSet;
import java.sql.Statement;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sermedical.Conector;
import sermedical.model.Paciente;
import sermedical.util.DateUtil;

/**
 *
 * @author tesco01
 */
public class ControlPaciente {
    private final ObservableList<Paciente> pacienteData = FXCollections.observableArrayList();
    /**
     * Returns the data as an observable list of Persons. 
     * @return
     */
    public ObservableList<Paciente> getPacienteData() {
        return pacienteData;
    }
    
    public void addPaciente(Paciente paciente){
        pacienteData.add(paciente);
    }
    /**
     * hace una consulta sql busca un paciente por consulta dada y lo carga. 
     * @param query
     */
    public void buscarPaciente(String query){
        //ArrayList<Integer> listDni = new ArrayList<Integer>();
        Conector.conecta();
        try {
            Statement s = Conector.connect.createStatement();
            ResultSet rs = s.executeQuery(query);
            while (rs.next()) { 
   
                pacienteData.add(new Paciente(rs.getInt("IdPaciente")
                        ,rs.getInt("Dni")
                        ,rs.getString("Apellido")
                        ,rs.getString("Nombre")
                        ,DateUtil.asLocalDate(rs.getDate("FechaNacimiento"))
                        ,rs.getString("sexo")
                        ,rs.getString("Pais")
                        ,rs.getString("Provincia")
                        ,rs.getString("Ciudad")
                        ,rs.getString("Direccion")
                        ,rs.getString("email")
                ));
                
            }
            for(Paciente p : pacienteData){
                System.out.println(p.getIdPaciente()  +" "+ p.getNombre()+ " "+ p.getEmail());
            }
            rs.close();
            Conector.close();
            
        }catch (Exception e) {
            System.out.println("Fallo al buscar Paciente!!");
        }
        
        
    }

}
