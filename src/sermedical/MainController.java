/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sermedical;

import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import sermedical.controlador.ControlPaciente;
import sermedical.model.Paciente;
import sermedical.util.DateUtil;

/**
 *
 * @author tesco01
 */
public class MainController implements Initializable {

    ControlPaciente controlPaciente = new ControlPaciente();
    private static Alert alert;
    private ObservableList<Paciente> personData = FXCollections.observableArrayList();
    @FXML
    private TableView<Paciente> pacienteTable;
    @FXML
    private TableColumn<Paciente, Integer> dniColumn;
    @FXML
    private TableColumn<Paciente, String> apellidoColumn;
    @FXML
    private TableColumn<Paciente, String> nombreColumn;
    @FXML
    private TableColumn<Paciente, LocalDate> nacimientoColumn;
    @FXML
    private TableColumn<Paciente, String> direccionColumn;
    @FXML
    private TableColumn<Paciente, String> ciudadColumn;
    @FXML
    private TableColumn<Paciente, String> provinciaColumn;
    @FXML
    private TextField paDniText;
    @FXML
    private TextField paNombreText;
    @FXML
    private TextField paApellidoText;
    @FXML
    private DatePicker paNacimientoDp;
    @FXML
    private ComboBox<String> paSexoCb;
    @FXML
    private ComboBox<String> emSexoCb;
    @FXML
    private TextField paPaisText;
    @FXML
    private TextField paProvinciaText;
    @FXML
    private TextField paCiudadText;
    @FXML
    private TextField paDireccionText;
    @FXML
    private TextField paEmailText;
    @FXML
    private TextField bpaDniText;
    @FXML
    private ComboBox<String> bpaServicioCb;
    @FXML
    private TextField tuCodPacienteText;
    @FXML
    private TextField tuNombreObraSoText;
    @FXML
    private TextField tuNumAfiliadoText;
    @FXML
    private TextField tuPlanObraSoText;
    @FXML
    private TextField tuTelObraSoText;
    @FXML
    private TextField tuEmailObraSoText;
    @FXML
    private TextField tuMontoText;
    @FXML
    private Button tuBtadd;
    @FXML
    private ComboBox<String> tuTipoCb;
    @FXML
    private ComboBox<String> tuServicioCb;
    @FXML
    private ComboBox<String> tuEspServicioCb;
    @FXML
    private ComboBox<String> tuProfecionalCb;
    @FXML
    private Button paBtAdd;
    @FXML
    private Button bpaBt;
    @FXML
    private void tuServiceSelect(ActionEvent event) {
        tuEspServicioCb.getItems().clear();
        int selectSer=tuServicioCb.getSelectionModel().getSelectedIndex()+1;
        Conector.conecta();
        try {
            Statement s = Conector.connect.createStatement();
            ResultSet rs = s.executeQuery("SELECT IdEspServicio,Nombre FROM EspServicio WHERE IdServicio="+selectSer);
            while (rs.next()) {
                tuEspServicioCb.getItems().add(rs.getString("Nombre"));
            }
            rs.close();
            Conector.close();

        } catch (Exception e) {
            System.out.println("Fallo al buscar Servicios y Turnos!!");
        }
    }

    @FXML
    private void tuBtaddAction(ActionEvent event) {
        int idObraS = 0;
        tuBtadd.setDisable(true);
        System.out.println("Cargando un nuevo turno");
        Conector.conecta();
        try {
            System.out.println("Agregando Obra Social");
            PreparedStatement st1 = Conector.connect.prepareStatement("insert into ObraSocial (NumeroAfiliado, Nombre, Plan, Telefono, Email) values (?,?,?,?,?)");
            st1.setInt(1, Integer.parseInt(tuNumAfiliadoText.getText()));
            st1.setString(2, tuNombreObraSoText.getText());
            st1.setString(3, tuPlanObraSoText.getText());
            st1.setString(4, tuTelObraSoText.getText());
            st1.setString(5, tuEmailObraSoText.getText());
            st1.execute();
            Conector.close();

            System.out.println("Ultimo idObraSocial");
            Conector.conecta();
            Statement s = Conector.connect.createStatement();
            ResultSet rs = s.executeQuery("SELECT MAX(IdObraSocial) AS id FROM ObraSocial");
            while (rs.next()) {
                idObraS = rs.getInt("id");
            }
            System.out.println("idObraSocial=" + idObraS);
            rs.close();
            Conector.close();

            System.out.println("Agregando Pago");
            //saca el valor del timer y el date
            java.util.Date utilDate = new java.util.Date(); //fecha actual
            long lnMilisegundos = utilDate.getTime();
            java.sql.Date sqlDate = new java.sql.Date(lnMilisegundos);

            Conector.conecta();
            PreparedStatement st2 = Conector.connect.prepareStatement("insert into Pago (IdPaciente, IdObraSocial, Monto, Fecha ) values (?,?,?,?)");
            st2.setInt(1, Integer.parseInt(tuCodPacienteText.getText()));
            st2.setInt(2, idObraS);
            st2.setString(3, tuPlanObraSoText.getText());
            st2.setString(4, tuMontoText.getText());
            st2.setDate(5, sqlDate);
            st2.execute();
            Conector.close();
            
            System.out.println("Agregando Turno");
            Conector.conecta();
            PreparedStatement st3 = Conector.connect.prepareStatement("insert into Turno (IdTipoTurno, IdServicio, LegajoProfecional, Fecha, HoraPedido, EstadoAtencion, IdPago) values (?,?,?,?,?,?,?)");
            st3.setInt(1, Integer.parseInt(tuTipoCb.getValue()));
            st3.setInt(2, Integer.parseInt(tuServicioCb.getValue()));
            //st3.setInt(3, Integer.parseInt());
            st3.execute();
            Conector.close();
            
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Error al conectarse a la BD motivo:");
            alert.setContentText(ex.getMessage());

            alert.showAndWait();
        }
        tuBtadd.setDisable(false);
    }

    @FXML
    private void bpaBtAction(ActionEvent event) {
        bpaBt.setDisable(true);
        controlPaciente.buscarPaciente("SELECT * FROM Paciente INNER JOIN  Persona ON Persona.Dni=Paciente.Dni"
                + " WHERE Persona.Dni='" + bpaDniText.getText() + "'");
        //controlPaciente.buscarPaciente("SELECT * FROM paciente INNER JOIN  persona ON persona.Dni=paciente.Dni");
        pacienteTable.setItems(controlPaciente.getPacienteData());
        bpaDniText.setText("");
        bpaBt.setDisable(false);
    }

    @FXML
    private void paBtAddAction(ActionEvent event) {
        // Nothing selected.
        paBtAdd.setDisable(true);
        System.out.println("Agregando Persona");
        //FALTA INVOCAR LA FUNCION QUE VALIDA LOS DATOS DE ENTRADA PERO YA FUE :)
        Conector.conecta();
        try {
            PreparedStatement st = Conector.connect.prepareStatement("insert into Persona (Dni, Apellido, Nombre, FechaNacimiento, sexo, Pais, Provincia, Ciudad, Direccion, email) values (?,?,?,?,?,?,?,?,?,?)");
            st.setInt(1, Integer.parseInt(paDniText.getText()));
            st.setString(2, paApellidoText.getText());
            st.setString(3, paNombreText.getText());
            st.setDate(4, new java.sql.Date(DateUtil.asDate(paNacimientoDp.getValue()).getTime()));
            st.setString(5, paSexoCb.getValue());
            st.setString(6, paPaisText.getText());
            st.setString(7, paProvinciaText.getText());
            st.setString(8, paCiudadText.getText());
            st.setString(9, paDireccionText.getText());
            st.setString(10, paEmailText.getText());
            st.execute();
            Conector.close();
            System.out.println("Agregando Paciente");
            Conector.conecta();
            PreparedStatement st2 = Conector.connect.prepareStatement("insert into Paciente (Dni) values (?)");
            st2.setInt(1, Integer.parseInt(paDniText.getText()));
            st2.execute();
            Conector.close();

            alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText("Se a conecto correctamente a la BD");
            alert.setContentText("Su paciente a sido agregado!");

            alert.showAndWait();

            controlPaciente.buscarPaciente("SELECT * FROM Paciente INNER JOIN  Persona ON Persona.Dni=Paciente.Dni"
                    + " WHERE Persona.Dni='" + paDniText.getText() + "'");
            pacienteTable.setItems(controlPaciente.getPacienteData());

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Error al conectarse a la BD motivo:");
            alert.setContentText(ex.getMessage());

            alert.showAndWait();
        }

        showPacienteDetails(null);
        paBtAdd.setDisable(false);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /*personData.add(new Paciente(1,3345177,"Hans", "Muster"));
         personData.add(new Paciente(2,36258478,"Ruth", "Mueller"));
         personData.add(new Paciente(3,45678923,"Heinz", "Kurz"));
         pacienteTable.setItems(personData);*/
        dniColumn.setCellValueFactory(cellData -> cellData.getValue().dniProperty().asObject());
        //TableColumn<Paciente, Number> adiColumn = new TableColumn<Paciente, Number>("Dni");
        apellidoColumn.setCellValueFactory(cellData -> cellData.getValue().apellidoProperty());
        nombreColumn.setCellValueFactory(cellData -> cellData.getValue().nombreProperty());
        nacimientoColumn.setCellValueFactory(cellData -> cellData.getValue().fechaNacimientoProperty());
        direccionColumn.setCellValueFactory(cellData -> cellData.getValue().direccionProperty());
        ciudadColumn.setCellValueFactory(cellData -> cellData.getValue().ciudadProperty());
        provinciaColumn.setCellValueFactory(cellData -> cellData.getValue().provinciaProperty());
        // Clear person details.
        showPacienteDetails(null);
        //TENGO QUE AGREGAR LOS SERVICIO Y LOS TURNOS ANTES DE EMPEZAR
        showServicioTurno();
        paSexoCb.getItems().addAll(
                "Masculino",
                "Femenino"
        );
        emSexoCb.getItems().addAll(
                "Masculino",
                "Femenino"
        );
        // Listen for selection changes and show the person details when changed.
        pacienteTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showPacienteDetails(newValue));
    }

    private void showServicioTurno() {
        Conector.conecta();
        try {
            Statement s = Conector.connect.createStatement();
            ResultSet rs = s.executeQuery("select * from Servicio");
            while (rs.next()) {
                tuServicioCb.getItems().add(rs.getString("Nombre"));
                bpaServicioCb.getItems().add(rs.getString("Nombre"));
            }
            rs = s.executeQuery("select * from TipoTurno");
            while (rs.next()) {
                tuTipoCb.getItems().add(rs.getString("Name"));
            }
            rs = s.executeQuery("SELECT LegajoProfecional AS legajo FROM Empleado WHERE idTipoEmpleado=2");
            while (rs.next()) {
                tuProfecionalCb.getItems().add(Integer.toString(rs.getInt("legajo")));
            }
            rs.close();
            Conector.close();

        } catch (Exception e) {
            System.out.println("Falla el autocompletar los combobox del inicio");
        }
    }

    /**
     * Fills all text fields to show details about the person. If the specified
     * person is null, all text fields are cleared.
     *
     * @param paciente the paciente or null
     */
    private void showPacienteDetails(Paciente paciente) {
        if (paciente != null) {
            // Fill the labels with info from the person object.
            paDniText.setText(Integer.toString(paciente.getDni()));
            paApellidoText.setText(paciente.getApellido());
            paNombreText.setText(paciente.getNombre());
            paNacimientoDp.setValue(paciente.getFechaNacimiento());
            paSexoCb.setValue(paciente.getSexo());
            paPaisText.setText(paciente.getPais());
            paProvinciaText.setText(paciente.getProvincia());
            paCiudadText.setText(paciente.getCiudad());
            paDireccionText.setText(paciente.getDireccion());
            paEmailText.setText(paciente.getEmail());
            tuCodPacienteText.setText(Integer.toString(paciente.getIdPaciente()));

        } else {
            paDniText.setText("");
            paApellidoText.setText("");
            paNombreText.setText("");
            paNacimientoDp.setValue(null);
            paSexoCb.setValue("");
            paPaisText.setText("Argentina");
            paProvinciaText.setText("Chaco");
            paCiudadText.setText("Resistencia");
            paDireccionText.setText("");
            paEmailText.setText("@");
            tuCodPacienteText.setText("");
        }

    }

}
