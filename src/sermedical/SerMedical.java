/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sermedical;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Optional;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Pair;

/**
 *
 * @author tesco01
 */
public class SerMedical extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        
        Parent root = FXMLLoader.load(getClass().getResource("Main.fxml"));

        Scene scene = new Scene(root);
        
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Login SerMedical v0.01");
        dialog.setHeaderText(" Ingrasa tus credenciales, para el acceso");
        // Set the icon (must be included in the project).
        dialog.setGraphic(new ImageView(this.getClass().getResource("/resources/pass.png").toString()));
        // Set the button types.
        ButtonType loginButtonType = new ButtonType("Login", ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        // Create the username and password labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField username = new TextField();
        username.setPromptText("Username");
        PasswordField password = new PasswordField();
        password.setPromptText("Password");

        grid.add(new Label("Username:"), 0, 0);
        grid.add(username, 1, 0);
        grid.add(new Label("Password:"), 0, 1);
        grid.add(password, 1, 1);

        // Enable/Disable login button depending on whether a username was entered.
        Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        loginButton.setDisable(true);

        // Do some validation (using the Java 8 lambda syntax).
        username.textProperty().addListener((observable, oldValue, newValue) -> {
            loginButton.setDisable(newValue.trim().isEmpty());
        });

        dialog.getDialogPane().setContent(grid);

        // Request focus on the username field by default.
        Platform.runLater(() -> username.requestFocus());

        // Convert the result to a username-password-pair when the login button is clicked.
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return new Pair<>(username.getText(), password.getText());
            }
            return null;
        });
        // Get the Stage.
        Optional<Pair<String, String>> result = dialog.showAndWait();
        String userG = "";
        String passwordG = "";
        /*result.ifPresent(usernamePassword -> {
         System.out.println("Username=" + usernamePassword.getKey() + ", Password=" + usernamePassword.getValue());
         });*/
        if (result.isPresent()) {
            userG = result.get().getKey();
            passwordG = result.get().getValue();
        }
        //Aca donde saco los datos de la bd para ver el permiso
        Conector.conecta();
        try {
            Statement s = Conector.connect.createStatement();
            ResultSet rs = s.executeQuery("SELECT * FROM Usuario");
            while (rs.next()) {
                if (userG.equals(rs.getString("NombreUsuario"))) {
                    if (passwordG.equals(rs.getString("Clave"))) {
                        System.out.println("La clave es correcta");
                        Image ico = new Image("/resources/logo.png");
                        stage.setTitle("SerMedical - Manager v0.01 ");
                        stage.getIcons().add(ico);
                        stage.setScene(scene);
                        stage.show();
                    } else {
                        System.out.println("Error en clave");
                    }
                }
            }
            rs.close();
            Conector.close();
        } catch (Exception e) {
            System.out.println("Fallo al leer Usuarios");
        }
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
