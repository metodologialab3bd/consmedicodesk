/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sermedical;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author betox
 */
public class Conector {

    public static Connection connect;

    /**
     * Metodo que permite conectar con la base de datos SQLite a travez de JDBC
     * y el driver Sqlite.
     *
     * @param null
     * @return void
     */
    public static void conecta() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection("jdbc:mysql://db4free.net:3306/sermedico", "sermedico", "beto1234");
            //connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/consmedico", "root", "");
            if (connect != null) {
                System.out.println("Conectado");
            }
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar a la base de datos\n" + ex.getMessage());
        } 
    }

    /**
     * Metodo que permite cerrar la base de datos SQLite a travez de JDBC y el
     * driver Sqlite.
     *
     * @param null
     * @return void
     */
    public static void close() {
        try {
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
